# DJson (Dependent JSON)

DJson is a library focused on providing tools to work with json objects in a dependently-typed setting.
JSON objects are parsed, their types are inferred and the result is a usable extensible record.

Records make use of a list-index describing their layout. They support operations such as adding fields
making sub-records and updating fields.
