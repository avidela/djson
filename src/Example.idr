module Example

import JSON.Parser
import Language.JSON
import Data.DPair

jsonString : String
jsonString = #"{"user": "John", "age": 23}"#

main : IO ()
main = do putStrLn (show jsonString)
          let Just (json ** parsed) = (parseFromString jsonString)
            | Nothing => pure ()
          pure ()
          print json

