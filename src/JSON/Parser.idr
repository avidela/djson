module JSON.Parser

import Language.JSON.Parser
import Language.JSON
import JSON.Record
import Data.Maybe
import Data.List
import Data.List.Quantifiers

public export
interface FromJSON ty where
  parseJSON : JSON -> Maybe ty

export
FromJSON String where
  parseJSON (JString x) = Just x
  parseJSON _ = Nothing

export
FromJSON Double where
  parseJSON (JNumber x) = Just x
  parseJSON _ = Nothing

export
FromJSON Nat where
  parseJSON (JNumber x) = Just (cast x)
  parseJSON _ = Nothing

export
FromJSON Int where
  parseJSON (JNumber x) = Just (cast x)
  parseJSON _ = Nothing

export
FromJSON t => FromJSON (List t) where
  parseJSON (JArray x) = traverse parseJSON x
  parseJSON _ = Nothing

%unbound_implicits off
mutual
  export
  parseSchema : (fields : List FieldType) -> (parsers : All (FromJSON . type) fields) -> JSON -> Maybe (Record fields)
  parseSchema fields ps (JObject xs) = parseObjFields fields ps xs
  parseSchema fields _ _ = Nothing

  parseObjFields : (fields : List FieldType) -> (parsers : All (FromJSON . type) fields) -> List (String, JSON) -> Maybe (Record fields)
  parseObjFields [] _ xs = Just [] -- all required fields are present, ignore the extra
  parseObjFields (key :! ty :: xs) (px :: ps) fs = do
    jval <- lookup key fs
    rval <- parseJSON @{px} jval
    rest <- parseObjFields xs ps fs
    Just $ key :-: rval :: rest

export
(fields : List FieldType) => (parsers : All (FromJSON . type) fields) => FromJSON (Record fields) where
  parseJSON = parseSchema fields parsers

export
parseJSONString : (json : String) -> (schema : List FieldType) -> (parsers : All (FromJSON . type) schema) => Maybe (Record schema)
parseJSONString json schema = JSON.parse json >>= parseJSON

UserFields : List FieldType
UserFields = [ "username" :! String , "age" :! Nat ]

test : Record [ "username" :! String , "age" :! Nat ]
test = [ "username" :-: "John", "age" :-: 19 ]

testParser : Maybe (Record UserFields)
testParser = parseJSONString "Helo" UserFields

testParser2 : Maybe (Record UserFields)
testParser2 = parseJSONString """
   { "username" : "John",
     "age" : 13 }
   """ UserFields
