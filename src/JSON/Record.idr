module JSON.Record

import Data.List.Quantifiers
import Data.String

export infix 3 :!:
export infix 8 :-:, :!

-- A Record Field is an instance of a field which requries the
-- name of the field and a value that inhabits the type of the field
public export
data RecordField : (field : String) -> (ty : Type) -> Type where
  (:-:) : (s : String) -> (_ : ty) -> RecordField s ty

||| A field type describe a record field by its name and its type
public export
record FieldType where
  constructor (:!)
  name : String
  type : Type

||| A record indexed by its fields, where each Field is a name and a type
public export
data Record : List FieldType -> Type where
  Nil : Record []
  (::) : RecordField s t -> Record ts -> Record ((s :! t) :: ts)

||| A proof that a record contains a certain field name
public export
data HasField : String -> List FieldType -> Type where
  Here : HasField f (f :! ty :: fs)
  There : HasField f fs -> HasField f (f' :: fs)

||| Return the type of a field inside a record
public export
GetType : {fs : _} -> HasField f fs -> Type
GetType Here {fs = f :! ty :: fs} = ty
GetType (There x) = GetType x

||| Project function takes a string and projects out the field with the corresponding name
public export
(.p) :  Record fs -> (f : String) -> {auto inField : HasField f fs} -> GetType inField
((f :-: val) :: y).p f {inField = Here} = val
(x :: xs).p f {inField = (There y)} = xs.p f {inField = y}

fromString : (f : String) -> Record fs -> {auto inField : HasField f fs} -> GetType inField
fromString f r = r.p f

toString : {fs : List FieldType} -> {ev : All (Show . FieldType.type) fs} -> Record fs -> List (String, String)
toString {ev = []} y = []
toString {ev = (x :: z)} ((s :-: y) :: rs) = (s, show y) :: toString {ev = z} rs

listShow : List (String, String) -> String
listShow ls  = unlines $ map (\(k, v) => "\{show k} : \{v}") ls

export
(fs : List FieldType) => (ev : All (Show . FieldType.type) fs) => Show (Record fs) where
  show rec = listShow $ toString {ev} rec

%unbound_implicits off
-----------------------------------------------------------------------------------
-- Tests and examples
-----------------------------------------------------------------------------------

userFields : List FieldType
userFields = [ "username" :! String
             , "age"      :! Nat]

userJSON : Record userFields
userJSON = ["username" :-: "john"
           , "age"     :-: 23
           ]

failing #"Mismatch between: "usernmae" and "username""#
  wrongName : Record userFields
  wrongName = ["usernmae" :-: "john"
             , "age"     :-: 23
             ]

failing "Can't find an implementation for FromString Nat"
  wrongType : Record userFields
  wrongType = ["username" :-: "john"
             , "age"     :-: "23"
             ]

username : String
username = userJSON.p "username"

username' : String
username' = "username" userJSON

failing #"Can't find an implementation for HasField "usernmae""#
  wrongField : String
  wrongField = userJSON.p "usernmae"

testFieldProj : Record.username === "john"
testFieldProj = Refl


